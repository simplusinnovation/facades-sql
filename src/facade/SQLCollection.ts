import { WriteOpsResults, WriteOpsResult, WriteOpsStats } from '../types/CollectionFacade';
import { SimpleSchema, ValidationOutput } from '@simplus/si-simple-schema';
import { define, setDialect, Table, ColumnDefinition, Column } from 'sql';
import { CollectionFacade, CollectionFacadeOptions } from '../types';
import { chirp, Filter, Update } from '@simplus/si-query-object';
import { omit, assign, defaults, isBoolean } from 'lodash';
import { SQLDatabaseStorage } from './SQLDatabaseStorage';
import { Database, ResultRow } from 'odbc';
import { ValidationError } from 'joi';
import { v4 } from 'uuid';

//////////////////////////////////////////////////////////////////////////////////////////////////
// CollectionFacade Interface requirements
//////////////////////////////////////////////////////////////////////////////////////////////////
export class SQLCollection<Model> extends SQLDatabaseStorage implements CollectionFacade<Model> {
	_options: CollectionFacadeOptions
	private _model: Table<string, Model| { [x: string]: {}; }> | null;
	private _schema: SimpleSchema

	constructor(options: CollectionFacadeOptions) {
		super(options)
		this._options = options
		this._model = null;
		this._schema = new SimpleSchema(defaults({_id: {dataType: String, primaryKey: true, name: '_id'}}, options.schema), options.dialect)
		this._loglevel = options ? (options.logLevel ? options.logLevel : 'info') : 'info'
		this._log.level(this._loglevel)
	}
	/**Connect
	 *
	 * Creates a connection to the database
	 *
	 * @example
	 * facade.connect().then((database) => {database.query()}).catch((err) => {new Error(err.message)})
	 *
	 * @returns : Promise<Database>
	 */
	connect():  Promise<Database> {
		return new Promise((resolve, reject) => {
			super.connect(this._options.connectionUrl).then(async (db) => {
				// First check connection to data base
				if (!this._db || !this._db.connected) {
					this._db = db;
					try {await this._db.openSync(this._options.connectionUrl); } catch (err) {this._log.error(err); reject(err); }
				}
				// Check is dailect is set otherwise default to postgres
				if (!this._options.dialect) {
					this._log.info(new Date(), 'Defaulted to prostgres dialect');
					this._options.dialect = 'postgres';
				}
				// Validate that the model exists
				if (!this._model) {
					setDialect(this._options.dialect);
					this._model = define({
						name: this._options.collection,
						columns: this._schema.define('sql', this._options.dialect) as {[x: string]: ColumnDefinition<string, {}>},
						schema: (this._options.schemaname && this._options.dialect !== 'sqlite') ? this._options.schemaname :  (this._options.dialect === 'postgres' ? 'public' : (this._options.dialect === 'mssql' ? 'dbo' : '')),
						dialect : this._options.dialect,
					});
				}
				// Create the table based on the model
				if (this._model) {
					try {
						await db.beginTransactionSync();
						if (this._options.schemaname && this._options.dialect !== 'sqlite') {
							const schemaQuery = await db.prepareSync(`CREATE SCHEMA IF NOT EXISTS ${this._options.schemaname}`)
							await schemaQuery.executeSync();
						}
						const tableQuery = await db.prepareSync(this._model.create().ifNotExists().toQuery().text);
						await tableQuery.executeSync();
						await db.commitTransactionSync();
						await db.endTransactionSync(true);
						this._log.info(new Date(), 'collection created => : ', this._options.collection);
						resolve(db);
					} catch (err) {this._log.error(new Date(), ' : ', err) ; reject(err); }
				}
			}).catch((err) => {this._log.error(new Date(), ' : ', err); reject(err); });
		});
	}
	/**createUniqueIndex
	 *
	 * Creates a unique index on a column
	 *
	 * @param column	:unique index column
	 * @param indexname	:optional name for index
	 *
	 * @example
	 * facade.closeConnection().then(() => {console.log('connection closed')}).catch((err) => {new Error(err.message)})
	 *
	 * @returns : Promise<void>
	 */
	dropCollection(): Promise<void> {
		return new Promise((resolve, reject) => {
			if (this._model && this._db) {
				const query = this._model.drop().ifExists().toQuery();

				const statement = this._db.prepareSync(query.text);
				statement.execute((err, result) => {
					if (err) {this._log.error(new Date(), ' : ', err); reject(err); }
					this._log.info(new Date(), ' : collection dropped  => : ', this._options.collection);
					resolve(result.closeSync());
				});
			}
			if (!this._model) { this._log.error(new Date() , ' : No collection table to drop'); reject('No collection table to drop'); } else if (!this._db) { this._log.error(new Date() , ' : Database not connected'); reject('Database not connected'); }
		});
	}
	/**cleanFilter
	 *
	 * replaces query paraeter placeholder fir sqlite with the correct "?"
	 *
	 * @param query		:query string
	 *
	 * @returns : string
	 */
	private cleanFilter(query: string): string {
		if (this._options.dialect === 'sqlite') {
			return query.replace(/\$/gi, '?')
		}
		if (this._options.dialect === 'postgres') { // Postgres parameter placeholder is infact ? and not $1
			return query.replace(/\$[0-9]*/gi, '?')
		}
		return query
	}
	/**cleanInput
	 *
	 * replaces query paraeter placeholder fir sqlite with the correct "?"
	 *
	 * @param values		:query string
	 *
	 * @returns : string
	 */
	// tslint:disable-next-line:no-any
	private cleanInput(values: any[]): any[] {
		if (this._options.dialect === 'postgres') {
			return values.map(v => isBoolean(v) ? v.toString() : v)
		}
		return values
	}
	/**Create
	 *
	 * inserts data into database
	 *
	 * @param model		:model object to insert as array
	 *
	 * @example
	 * facade.create({name: Athenkosi, surname: Mase}).then((data) => {console.log('model inserted')}).catch((err) => {new Error(err.message)})
	 *
	 * @returns : Promise<ResultRow[]>
	 */
	create(...model: Model[] ): Promise<WriteOpsResults<Model>> {
		return new Promise(async (resolve, reject) => {
			// create unique id for each row on insert
			model.map((m: {}) => {defaults(m, {_id: v4()})})
			const validation = (this._options.validation) ? this._schema.validate(...model) : {status: true, content: model, error: null} as ValidationOutput<Model>
			if (!validation.status) {
				this._log.error(new Date(), ' : ', validation.error)
				reject((validation.error as ValidationError).details)
			}
			if (this._model && this._db) {
				try {
					await this._db.beginTransactionSync();

					const query = this._model.insert(validation.content as Model[]).toQuery();
					query.text = this.cleanFilter(query.text);
					query.values = this.cleanInput(query.values)

					const statement = await this._db.prepareSync(query.text);
					await statement.bindSync(query.values);
					await statement.executeSync(query.values).closeSync();

					await this._db.commitTransactionSync();
					await this._db.endTransactionSync(false);

					this._log.info(new Date(), ' : ', model.length, ' items created in ', this._options.collection);
					resolve({stats: {error: false, created: model.length, errors: 0, empty: 0, updated: 0, removed: 0, ignored: 0, code: 200}, data: model});
				} catch (error) {this._log.error(new Date() , error); reject(error)}
			} else { this._log.error(new Date() , ' : No collection table to drop\n'); reject('No collection table to drop')}
		});
	}
	/**DuplicateById
	 *
	 * creates a duplicate entry with a new unique id.
	 *
	 * @param id		:unique id of element to duplicate
	 *
	 * @example
	 * facade.duplicateById('12345').then((data) => {console.log('model duplicated')}).catch((err) => {new Error(err.message)})
	 *
	 * @returns : Promise<ResultRow>
	 */
	duplicateById(id: string): Promise<WriteOpsResult<Model>> {
		return new Promise((resolve, reject) => {
			this.findById(id)
				.then((model: ResultRow) => {
					this.create(omit(model, ['_id']) as Model)
					.then((data) => {resolve({stats: data.stats, data: data.data[0]} as WriteOpsResult<Model>)})
					.catch(reject)
				}).catch(reject)
		})
	}
	/**Update
	 *
	 * update element(s) with new model information based on filter.
	 *
	 * @param filter		:filter parameters to segment items
	 * @param model			:rule to update model
	 *
	 * @example
	 * facade.update({name: {$eq : 'Athenkosi'}}, {$set :{name: 'Yehudi'}}).then((data) => {console.log('name updated')}).catch((err) => {new Error(err.message)})
	 *
	 * @returns : Promise<UpdateWriteOpResult>
	 */
	update(filter: Filter, model: Update): Promise<WriteOpsResults<Model>> {
		return new Promise((resolve, reject) => {
			if (this._model && this._db) {
				const F = chirp.Filter.SQL(filter);
				const U = chirp.Update.SQL(model, {prefix: ''});
				let mo: object = {}

				// This is a temporal solution until I correct the query engine
				const correction = U ? U.toString().split(' , ') : []
				correction.forEach((node) => {
					const ret = {} as {[field: string]: string}
					const char = node.split(' = ')
					ret[char[0]] = char[1]
					mo = assign(mo, ret)
				})
				// End of temporal correction

				const query  = this._model.update(mo).where(F ? F.toString() : []).toQuery();
				query.text = this.cleanFilter(query.text);

				const statement = this._db.prepareSync(query.text);
				statement.execute(query.values, (err1: Error, result) => {
					if (err1) {this._log.error(new Date(), ' : ', err1); reject(err1)}
					result.fetchAll((err2: Error, data) => {
						if (err2) {this._log.error(new Date(), ' : ', err2); reject(err2)}
						this._log.info(new Date(), ' : ', data)
						resolve({stats: {error: false, code: 200}, data: data as Model[]});
					})
				});
			}
			if (!this._model) { this._log.error(new Date() , ' : No collection table to drop'); reject('No collection table to drop'); } else if (!this._db) { this._log.error(new Date() , ' : Database not connected'); reject('Database not connected'); }
		});
	}
	/**Update By Id
	 *
	 * update element with new model information based on id.
	 *
	 * @param id			:element unique id
	 * @param model			:rule to update model
	 *
	 * @example
	 * facade.updateById('12345', {$set :{name: 'Yehudi'}}).then((data) => {console.log('name updated')}).catch((err) => {new Error(err.message)})
	 *
	 * @returns : Promise<UpdateWriteOpResult>
	 */
	updateById(id: string, model: Update): Promise<WriteOpsResult<Model>> {
		return new Promise((resolve, reject) => {
			if (this._model && this._db) {
				const U = chirp.Update.SQL(model, {prefix: ''});
				let mo: object = {}

				// This is a temporal solution until I correct the query engine
				const correction = U ? U.toString().split(' , ') : []
				correction.forEach((node) => {
					const ret = {} as {[field: string]: string}
					const char = node.split(' = ')
					ret[char[0]] = char[1]
					mo = assign(mo, ret)
				})
				// End of temporal correction

				const query = this._model.update(mo).where((this._model as Table<string, Model&{_id: string}>)._id.equals(id)).toQuery();
				query.text = this.cleanFilter(query.text);

				const statement = this._db.prepareSync(query.text);
				statement.execute(query.values, (err1: Error, result) => {
					if (err1) {this._log.error(new Date(), ' : ', err1); reject(err1)}
					result.fetchAll((err2: Error, data) => {
						if (err2) {this._log.error(new Date(), ' : ', err2); reject(err2); }
						this._log.info(new Date(), ' : ', data)
						resolve({stats: {error: false, updated: 1, code: 200}, data: data[0] as Model});
					})
				});
			}
			if (!this._model) { this._log.error(new Date() , ' : No collection table to drop'); reject('No collection table to drop'); } else if (!this._db) { this._log.error(new Date() , ' : Database not connected'); reject('Database not connected'); }
		});
	}
	/**Find One
	 *
	 * find first element to appear based on filter
	 *
	 * @param filter		:filter parameters to segment items
	 *
	 * @example
	 * facade.findOne({name: {$eq : 'Athenkosi'}}).then((data) => {console.log(data)}).catch((err) => {new Error(err.message)})
	 *
	 * @returns : Promise<ResultRow>
	 */
	findOne(filter: Filter): Promise<Model> {
		return new Promise((resolve, reject) => {
			if (this._model && this._db) {
				const F = chirp.Filter.SQL(filter);

				const query = this._model.select(this._model.star()).from(this._model).where(F ? F.toString() : []).limit(1).toQuery();
				query.text = this.cleanFilter(query.text);

				const statement = this._db.prepareSync(query.text);
				statement.execute(query.values, (err1: Error, result) => {
					if (err1) {this._log.error(new Date(), ' : ', err1); reject(err1); }
					result.fetchAll((err2: Error, data) => {
						if (err2) {this._log.error(new Date(), ' : ', err2); reject(err2); }
						this._log.info(new Date(), ' : ', data[0] || {})
						resolve(data[0] as Model || {});
					});
				});
			}
			if (!this._model) { this._log.error(new Date() , ' : No collection table to drop'); reject('No collection table to drop'); } else if (!this._db) { this._log.error(new Date() , ' : Database not connected'); reject('Database not connected'); }
		});
	}
	/**Find By Id
	 *
	 * find element based on the unique id
	 *
	 * @param id		:unique identifier string
	 *
	 * @example
	 * facade.findById('1234').then((data) => {console.log(data)}).catch((err) => {new Error(err.message)})
	 *
	 * @returns : Promise<ResultRow>
	 */
	findById(id: string): Promise<Model> {
		return new Promise((resolve, reject) => {
			if (this._model && this._db) {

				const query = this._model.select(this._model.star()).from(this._model).where((this._model as Table<string, Model&{_id: string}>)._id.equals(id)).toQuery();
				query.text = this.cleanFilter(query.text);

				const statement = this._db.prepareSync(query.text);
				statement.execute(query.values, (err1: Error, result) => {
					if (err1) {this._log.error(new Date(), ' : ', err1); reject(err1); }
					result.fetchAll((err2: Error, data) => {
						if (err2) {this._log.error(new Date(), ' : ', err2); reject(err2); }
						this._log.info(new Date(), ' : ', data[0] || {})
						resolve(data[0] as Model || {});
					});
				});
			}
			if (!this._model) { this._log.error(new Date() , ' : No collection table to drop'); reject('No collection table to drop'); } else if (!this._db) { this._log.error(new Date() , ' : Database not connected'); reject('Database not connected'); }
		});
	}
	/**Find any
	 *
	 * find any or all elements in collection
	 *
	 * @param filter		:filter parameters to segment items
	 *
	 * @example
	 * facade.findById({month: {$eq: 'January'}}).then((data) => {console.log(data)}).catch((err) => {new Error(err.message)})
	 *
	 * @returns : Promise<ResultRow[]>
	 */
	find(filter: Filter): Promise<Model[]> {
		return new Promise((resolve, reject) => {
			if (this._model && this._db) {
				const F = chirp.Filter.SQL(filter);

				const query = this._model.select(this._model.star()).where(F ? F.toString() : []).from(this._model).toQuery();
				query.text = this.cleanFilter(query.text);

				const statement = this._db.prepareSync(query.text);
				statement.execute(query.values, (err1: Error, result) => {
					if (err1) {this._log.error(new Date(), ' : ', err1); reject(err1); }
					result.fetchAll((err2: Error, data) => {
						if (err2) {this._log.error(new Date(), ' : ', err2); reject(err2); }
						this._log.info(new Date(), ' : ', data);
						resolve(data as Model[]);
					});
				});
			}
			if (!this._model) { this._log.error(new Date() , ' : No collection table to drop'); reject('No collection table to drop'); } else if (!this._db) { this._log.error(new Date() , ' : Database not connected'); reject('Database not connected'); }
		});
	}
	/**Remove any
	 *
	 * delete any or all elements in collection
	 *
	 * @param filter		:filter parameters to segment items
	 *
	 * @example
	 * facade.remove({month: {$eq: 'January'}}).then((res) => {console.log('items removed')}).catch((err) => {new Error(err.message)})
	 *
	 * @returns : Promise<DeleteWriteOpResultObject>
	 */
	remove(filter: Filter): Promise<WriteOpsStats>  {
		return new Promise((resolve, reject) => {
			if (this._model && this._db) {
				const F = chirp.Filter.SQL(filter);

				const query = this._model.delete().where(F ? F.toString() : []).toQuery();
				query.text = this.cleanFilter(query.text);

				const statement = this._db.prepareSync(query.text);
				statement.execute(query.values, (err1: Error, result) => {
					if (err1) {this._log.error(new Date(), ' : ', err1); reject(err1); }
					result.fetchAll((err2: Error, data) => {
						if (err2) {this._log.error(new Date(), ' : ', err2); reject(err2); }
						this._log.info(new Date(), ' : ', data);
						resolve({error: false, removed: data.length, code: 200 });
					});
				});
			}
			if (!this._model) { this._log.error(new Date() , ' : No collection table to drop'); reject('No collection table to drop'); } else if (!this._db) { this._log.error(new Date() , ' : Database not connected'); reject('Database not connected'); }
		});
	}
	/**Remove by id
	 *
	 * delete element based on the unique identifier
	 *
	 * @param id		:unique identifier string
	 *
	 * @example
	 * facade.removeById('1234').then((res) => {console.log('item removed')}).catch((err) => {new Error(err.message)})
	 *
	 * @returns : Promise<DeleteWriteOpResultObject>
	 */
	removeById(id: string): Promise<WriteOpsStats> {
		return new Promise((resolve, reject) => {
			if (this._model && this._db) {
				const query = this._model.delete().where((this._model as Table<string, Model&{_id: string}>)._id.equals(id)).toQuery();
				query.text = this.cleanFilter(query.text);

				const statement = this._db.prepareSync(query.text);
				statement.execute(query.values, (err1: Error, result) => {
					if (err1) {this._log.error(new Date(), ' : ', err1); reject(err1); }
					result.fetchAll((err2: Error, data) => {
						if (err2) {this._log.error(new Date(), ' : ', err2); reject(err2); }
						this._log.info(new Date(), ' : ', data);
						resolve({error: false, removed: 1, code: 200 });
					});
				});
			} else if (!this._model) { this._log.error(new Date() , ' : No collection table to drop'); reject('No collection table to drop'); } else if (!this._db) { this._log.error(new Date() , ' : Database not connected'); reject('Database not connected'); }
		});
	}
	/**createUniqueIndex
	 *
	 * Creates a unique index on a column
	 *
	 * @param column	:unique index column
	 * @param indexname	:optional name for index
	 *
	 * @example
	 * facade.closeConnection().then(() => {console.log('connection closed')}).catch((err) => {new Error(err.message)})
	 *
	 * @returns : Promise<void>
	 */
	createUniqueIndex(column: string): Promise<void> {
		return new Promise((resolve, reject) => {
			if (this._db && this._model) {
				try {
					this._db.beginTransactionSync()
					// tslint:disable-next-line:no-any
					const indexColumn = this._model.columns.filter((c: Column<any, any>) => c.name === column)
					const indexQuery = this._model.indexes().create().unique().on(indexColumn).using('indexname').toQuery()
					const statement = this._db.prepareSync(indexQuery.text)
					statement.executeSync().closeSync()
					this._db.commitTransactionSync()
					this._db.endTransactionSync(false)

					this._log.info(new Date(), ' : Created Unique Index on column ', this._options.collection, '.', column)
					resolve()
				} catch (err) {; resolve()}
			} else {this._log.error(new Date(), ' : Database not connected'); reject('Database not connected')}
		})
	}
}

export default SQLCollection;