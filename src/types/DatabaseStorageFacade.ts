import { WriteOpsResults } from './CollectionFacade';

/**
 * Facade
 */
export interface DatabaseStorageFacade<Databases> {
	/**Connect
	 *
	 * Creates a connection to the database
	 *
	 * @example
	 * facade.connect().then((database) => {database.query()}).catch((err) => {new Error(err.message)})
	 *
	 * @returns : Promise<Database>
	 */
	connect (connectionUrl: string): Promise<Databases>
	/**closeConnection
	 *
	 * Closes connection to the database
	 *
	 * @example
	 * facade.closeConnection().then(() => {console.log('connection closed')}).catch((err) => {new Error(err.message)})
	 *
	 * @returns : Promise<void>
	 */
	closeConnection (): Promise<void>
	/**custom query
	 *
	 * allows for custom SQL string expression for complex queries
	 *
	 * @param query			:custom query string
	 * @param params		:optional query parameters for query
	 *
	 * @example
	 * facade.query('SELECT * FROM TABLE WHERE i = 1).then((data) => {console.log(data)}).catch((err) => {new Error(err.message)})
	 *
	 * @returns : Promise<ResultRow[]>
	 */
	query(query: string, params: string[]|number[]|boolean[]): Promise<WriteOpsResults<{}>>
}
export default DatabaseStorageFacade