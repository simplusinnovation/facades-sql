export { CollectionFacade } from './CollectionFacade';
export { DatabaseStorageFacade } from './DatabaseStorageFacade';
export { CollectionFacadeOptions, DatabaseStorageFacadeOptions } from './FacadeOptions';
export type Index = 'Unique';